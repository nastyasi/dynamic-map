define(['jquery', 'underscore', 'backbone', 'models/map'], 
function($, _, Backbone, Map) {

	var appMap, appData;

	var initialize = function(appData) {
		appData = JSON.parse(appData);
		
		appMap = new Map(appData, {silent: true}, this);


		//Router.initialize();
	};

	return {
		initialize : initialize
	};
}); 