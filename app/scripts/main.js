require.config({
  paths: {
    'jquery': 'vendor/jquery/dist/jquery',
    'underscore': 'vendor/underscore/underscore',
    'backbone': 'vendor/backbone/backbone',
    'kineticjs': 'vendor/kineticjs/kinetic',
    'text' : 'vendor/requirejs-text/text',
    'utils' : 'helpers/utils'
  }
});

require([
	'app',
	'text!helpers/app_data.json'
], function(App, data){
	App.initialize(data);
});