define(['jquery', 'underscore', 'backbone', 'kineticjs'], 
function($, _, Backbone, Kinetic) {

	var loadImages = function(sources, imgArr, callback) {

		$('<div id="loader">Идет загрузка изображений...<br />').appendTo('body');

		var loadedImages = 0, numImages = 0;
		for (var src in sources) {
			numImages++;
		}
		for (var src in sources) {
			imgArr[src] = new Image();
			imgArr[src].onload = function() {
				if (++loadedImages >= numImages) {
					callback();
					$('#loader').remove();
				}
			};
			imgArr[src].onerror = function() {
				if (++loadedImages >= numImages) {
					callback();
					$('#loader').remove();
				}
			};
			imgArr[src].src = sources[src];
		}
	};
	
	var getRandomInt = function(min, max) {
	    return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	return {
		loadImages : loadImages,
		getRandomInt : getRandomInt
	};
}); 