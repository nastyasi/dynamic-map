define(['jquery', 'underscore', 'backbone', 'kineticjs', 'utils'], 
function($, _, Backbone, Kinetic, Utils) {

	var Entities = Backbone.Model.extend({
		
		entitiesImg : {},
		all_objects : [],
		objectsGroup : {},
		
		initialize : function(options, config, parent) {
			Backbone.Model.prototype.initialize.call(this, options, config, parent);
			this.parent = parent;
			this.all_objects = this.parent.get('map_objects');
		},
		
		place: function(x_start, y_start, x_finish, y_finish){
			Utils.loadImages(this.parent.get('img_paths').objects, this.entitiesImg, function() {debugger;
				this.show(x_start, y_start, x_finish, y_finish);
			}.bind(this));
		},
		
		show: function(x_start, y_start, x_finish, y_finish){debugger;
			//упорядочиваем объекты по игреку для правильного вывода на изометрической карте: те, что на переднем плане, перекрывают тех, что на заднем плане
			var all_objects_sorted = _.sortBy(this.all_objects, function(el){ return el.y; });
		
			this.objectsGroup = new Kinetic.Group();
		
			$.each(all_objects_sorted, function(ix, el){debugger;
		
				var objectCoordX = el.x, objectCoordY = el.y;
		
				if((objectCoordX>=x_start&&objectCoordX<=x_finish)&&(objectCoordY>=y_start&&objectCoordY<=y_finish)) { 
		
					if (el.y % 2) {
						var offset_x = this.parent.tileW / 2;
					} else {
						var offset_x = 0;
					}
		
					if(_.has(el, 'visual_type') && this.entitiesImg[el.visual_type]) {
		
						var xpos = (el.x * this.parent.tileW) + offset_x - (this.entitiesImg[el.visual_type].width)/2 + this.parent.tileW/2;
						var ypos = el.y * this.parent.tileH / 2 - (this.entitiesImg[el.visual_type].height - this.parent.tileH);
		
						var map_object = new Kinetic.Image({
							x : xpos,
							y : ypos,
							image : this.entitiesImg[el.visual_type],
							width : this.entitiesImg[el.visual_type].width,
							height : this.entitiesImg[el.visual_type].height,
							name : 'map_object',
							boardCoordX : el.x,
							boardCoordY : el.y,
							id : 'map_object'+el.x+'-'+el.y
						});	
		
						var original_zindex;
		
						map_object.on('mouseover', function(){				
							this.setOpacity('0.5');
							original_zindex = this.getZIndex();
							this.setZIndex(1);
							this.parent.tilesLayer.draw();
						});
		
		
						this.objectsGroup.add(map_object);
						//map_object.setAttrs({ original_zindex : map_object.getZIndex() });
		
					}
		
					//если объект - герой
					else {
						/*var xpos = (objectCoordX * this.parent.tileW) + offset_x + this.parent.tileW/4;
						var ypos = objectCoordY * this.parent.tileH / 2 - (human_sprite_size/4);
		
						var human = new Kinetic.Sprite({
							x : xpos,
							y : ypos,
							image : humanSpriteImage,
							animation : 'stand',
							animations : humanAnimations,
							frameRate : 7,
							boardCoordX : objectCoordX,
							boardCoordY : objectCoordY,
							human_id : el.id,
							name: 'human', 
							action_points: el.action_points
						});
		
						human.on('mouseover', function() {
							$('#main').css('cursor', 'pointer');
						});
				
						human.on('mouseout', function() {
							$('#main').css('cursor', 'default');
						});
						
						human.on('click', function() {
							$('#main').css('cursor', 'default');
		
							var humans_on_board = tilesLayer.get('.human');
							_.each(humans_on_board, function(human_on_board){
								human_on_board.off('click mouseover mouseout');
							});
							stage.setDraggable(false);			
							chooseForMoving(this);	
						});
		
			         	this.objectsGroup.add(human);*/
					}
				}
			}.bind(this));
		
			this.parent.tilesLayer.add(this.objectsGroup);
		}
		
	});
	return Entities;
}); 