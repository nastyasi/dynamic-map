define(['jquery', 'underscore', 'backbone', 'kineticjs', 'utils', 'models/Entities'], function($, _, Backbone, Kinetic, Utils, Entities) {
	var Map = Backbone.Model.extend({

		cameraY : 0,
		cameraX : 0,
		tileH : 64,
		tileW : 128,
		stage : null,
		tilesLayer : null,
		tileImg : {},
		entities : {},

		defaults : {
			name : "Random Map",
		},

		initialize : function(options, config, parent) {
			Backbone.Model.prototype.initialize.call(this, options, config, parent);

			this.initStage();
			this.initTilesLayer();

			this.entities = new Entities({}, {silent: true}, this);

			this.set('tilesTypes', _.keys(this.get('img_paths').tiles));

			Utils.loadImages(this.get('img_paths').tiles, this.tileImg, function() {
				this.show();
			}.bind(this));
		},

		show : function() {
			var x_start = (Math.round(this.cameraX / this.tileW) > 0 ? Math.round(this.cameraX / this.tileW) : 0), x_finish = Math.round((this.stage.getWidth() + this.tileW) / this.tileW) + Math.round(this.cameraX / this.tileW), y_start = (Math.round(this.cameraY / this.tileH) > 0 ? Math.round(this.cameraY / this.tileH) : 0), y_finish = Math.round((this.stage.getHeight() + this.tileH) / (this.tileH / 2)) + Math.round(this.cameraY / this.tileH);

			var mapAreaTiles = this.generateMapAreaTiles(x_start, y_start, x_finish, y_finish);
			this.drawMapArea(mapAreaTiles);

			this.entities.place(x_start, y_start, x_finish, y_finish);

			this.setMapEvents();
		},

		initStage : function() {
			this.stage = new Kinetic.Stage({
				container : "main",
				width : $(document).width(),
				height : $(document).height(),
				draggable : true
			});
		},

		initTilesLayer : function() {
			this.tilesLayer = new Kinetic.Layer({
				id : 'tilesLayer'
			});
		},

		generateMapAreaTiles : function(x_start, y_start, x_finish, y_finish) {
			var tilesObjects = [];

			for (var y = y_start; y <= y_finish; y++) {
				for (var x = x_finish; x >= x_start; x--) {
					tilesObjects.push({
						x : x,
						y : y,
						visual_type : this.get('tilesTypes')[Math.floor(Math.sqrt(x * y) / 10) >= this.get('tilesTypes').length ? 0 : Math.floor(Math.sqrt(x * y) / 10)],
						type : 7
					});
				}
			}

			return tilesObjects;
		},

		drawMapArea : function(mapAreaTiles) {

			$.each(mapAreaTiles, function(ix, el) {

				var offset_x = (el.y % 2) ? this.tileW / 2 : 0, xpos = (el.x * this.tileW) + offset_x - (this.tileImg[el.visual_type].width - this.tileW), ypos = el.y * this.tileH / 2 - (this.tileImg[el.visual_type].height - this.tileH);

				var tile = new Kinetic.Image({
					x : xpos,
					y : ypos,
					original_y : ypos,
					image : this.tileImg[el.visual_type],
					original_image : this.tileImg[el.visual_type],
					width : this.tileImg[el.visual_type].width,
					height : this.tileImg[el.visual_type].height,
					original_height : this.tileImg[el.visual_type].height,
					name : 'tile',
					boardCoordX : el.x,
					boardCoordY : el.y,
					id : 'tile' + el.x + '-' + el.y,
					db_id : el.id
				});

				this.tilesLayer.add(tile);
			}.bind(this));

			//drawObjects();

			this.stage.add(this.tilesLayer);

			//drawMinimap();
		},

		reDrawMapArea : function(mapAreaTiles) {
			this.tilesLayer.removeChildren();
			this.drawMapArea(mapAreaTiles);
		},

		setMapEvents : function() {
			document.onselectstart = function() {
				return false;
			};

			this.stage.on('dragstart', function() {
				$('body').css('cursor', 'move');
			});

			this.stage.on('dragend', function() {
				$('body').css('cursor', 'default');
			});

			var that = this;

			this.stage.on('dragend', function() {

				var movedX = this.getX(), movedY = this.getY(), y_start = (movedY > 0) ? 0 : -Math.round(movedY / that.tileH), y_finish = Math.round((that.stage.getHeight() + that.tileH) / (that.tileH / 2)) + y_start * 2, x_start = (movedX > 0) ? 0 : -Math.round(movedX / that.tileW), x_finish = Math.round((that.stage.getWidth() + that.tileW) / that.tileW) + x_start, mapAreaTiles = that.generateMapAreaTiles(x_start, y_start, x_finish, y_finish);

				that.reDrawMapArea(mapAreaTiles);
			});
		}
	});
	return Map;
});
